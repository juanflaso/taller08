FLAGS = gcc -Wall -c -I
bin/taller8: obj/insertion.o obj/comp1.o obj/comp2.o obj/comp_extrano.o obj/taller8.o
	gcc -Wall $^ -o $@
obj/insertion.o: src/insertion.c
	$(FLAGS) include/ $< -o $@
obj/comp1.o: src/comp1.c
	$(FLAGS) include/ $< -o $@
obj/comp2.o: src/comp2.c
	$(FLAGS) include/ $< -o $@
obj/comp_extrano.o: src/comp_extrano.c
	$(FLAGS) include/ $< -o $@
obj/taller8.o: src/taller8.c
	$(FLAGS) include/ $< -o $@
	
.PHONY: clean 	
clean:
	rm bin/taller8